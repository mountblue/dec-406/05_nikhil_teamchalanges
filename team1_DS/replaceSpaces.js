let input = "    hellowhaskdsa    ";
let noSpaceInput = input.split(" ");

let numOfSpaces = noSpaceInput.length - 1;

for (let i = 0; i < numOfSpaces; i++) {
  noSpaceInput.unshift(" ");
}

console.log(noSpaceInput.join(""));
