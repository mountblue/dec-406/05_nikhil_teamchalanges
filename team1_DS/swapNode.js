class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  add(data) {
    let node = new Node(data);

    if (this.head) {
      let currentNode = this.head;

      while (currentNode.next) {
        currentNode = currentNode.next;
      }
      currentNode.next = node;
    } else {
      this.head = node;
    }
    this.size += 1;
    return null;
  }

  insertAt(data, index) {
    let i = 0;
    if (index > this.size) {
      console.log("index out of bound");
      return null;
    }
    let node = new Node(data);

    if (index == 0) {
      node.next = this.head;
      this.head = node;
    } else {
      let presentNode = this.head;
      while (presentNode.next) {
        i++;

        if (index == i) {
          node.next = presentNode.next;
          presentNode.next = node;
        }
        presentNode = presentNode.next;
      }
    }
  }

  swap(n1, n2) {
    let prevNode1 = null;
    let prevNode2 = null;
    let temp1;
    let temp2;
    if (n1 == n2) {
      return;
    }

    temp1 = this.head;
    while (temp1 != null && temp1.data != n1) {
      //   console.log(temp1.data);
      prevNode1 = temp1;
      temp1 = temp1.next;
    }

    temp2 = this.head;
    while (temp2 != null && temp2.data != n2) {
      prevNode2 = temp2;
      temp2 = temp2.next;
    }

    if (temp1 === null || temp2 === null) {
      return;
    }

    if (temp1 != null) {
      prevNode1.next = temp2;
    } else {
      this.head = temp2;
    }

    if (temp2 != null) {
      prevNode2.next = temp1;
    } else {
      this.head = temp1;
    }

    let temp = temp1.next;
    temp1.next = temp2.next;
    temp2.next = temp;
  }

  printElement() {
    if (this.size == 0) {
      return "empty list";
    } else if (this.size == 1) {
      return this.head.data;
    } else {
      let presentNode = this.head;
      let arr = [];
      while (presentNode) {
        arr.push(presentNode.data);
        presentNode = presentNode.next;
      }
      return arr;
    }
  }
}

let linked_list = new LinkedList();
linked_list.add(5);
linked_list.add(7);
linked_list.add(1);
linked_list.add(3);
linked_list.add(2);
linked_list.add(9);
linked_list.add(4);
linked_list.add(10);

console.log(linked_list.printElement());

linked_list.swap(7, 4);

console.log(linked_list.printElement());
