class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  add(data) {
    let node = new Node(data);

    if (this.head) {
      let currentNode = this.head;

      while (currentNode.next) {
        currentNode = currentNode.next;
      }
      currentNode.next = node;
    } else {
      this.head = node;
    }
    this.size += 1;
    return null;
  }

  printElement() {
    if (this.size == 0) {
      return "empty list";
    } else if (this.size == 1) {
      return this.head.data;
    } else {
      let presentNode = this.head;
      let arr = [];
      while (presentNode) {
        arr.push(presentNode.data);
        presentNode = presentNode.next;
      }
      return arr;
    }
  }

  reverse(k) {
    let currentNode = this.head.next;
    let prevNode = this.head;

    for (let i = 0; i < k; i++) {
      console.log("sd");
      prevNode.next = currentNode.next;
      currentNode.next = this.head;
      this.head = currentNode;
      currentNode = prevNode.next;
    }
    let rootNode = currentNode;
    prevNode = currentNode;
    currentNode = currentNode.next;
    while (currentNode) {
      prevNode.next = currentNode.next;
      currentNode.next = rootNode;
      rootNode = currentNode;
      currentNode = prevNode.next;
    }
  }
}

let linked_list = new LinkedList();
linked_list.add(1);
linked_list.add(3);
linked_list.add(5);
linked_list.add(2);
linked_list.add(9);
linked_list.add(4);
linked_list.add(6);
linked_list.add(7);

console.log(linked_list.printElement());
linked_list.reverse(3);
console.log(linked_list.printElement());
