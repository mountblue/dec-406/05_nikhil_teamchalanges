class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  add(data) {
    let node = new Node(data);

    if (this.head) {
      let currentNode = this.head;

      while (currentNode.next) {
        currentNode = currentNode.next;
      }
      currentNode.next = node;
    } else {
      this.head = node;
    }
    this.size += 1;
    return null;
  }

  printElement() {
    if (this.size == 0) {
      return "empty list";
    } else if (this.size == 1) {
      return this.head.data;
    } else {
      let presentNode = this.head;
      let arr = [];
      while (presentNode) {
        arr.push(presentNode.data);
        presentNode = presentNode.next;
      }
      return arr;
    }
  }

  skipAndDel(m, n) {
    let currentNode = this.head;
    let count;
    while (currentNode) {
      for (count = 1; count < m && currentNode != null; count++) {
        currentNode = currentNode.next;
      }
      if (currentNode == null) {
        return;
      }

      let temp = currentNode.next;
      let prev = currentNode;
      for (count = 0; count < n && temp != null; count++) {
        let t = temp;
        temp = temp.next;
        prev.next = temp.next;
      }
      currentNode.next = temp;
      currentNode = temp;
    }
  }
}

let linked_list = new LinkedList();
linked_list.add(1);
linked_list.add(3);
linked_list.add(5);
linked_list.add(2);
linked_list.add(9);
linked_list.add(4);

linked_list.skipAndDel(3, 2);
console.log(linked_list.printElement());
