class Node {
  constructor(data) {
    this.data = data;
    this.next = null;
  }
}

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  add(data) {
    let node = new Node(data);

    if (this.head) {
      let currentNode = this.head;

      while (currentNode.next) {
        currentNode = currentNode.next;
      }
      currentNode.next = node;
    } else {
      this.head = node;
    }
    this.size += 1;
    return null;
  }

  printElement() {
    if (this.size == 0) {
      return "empty list";
    } else if (this.size == 1) {
      return this.head.data;
    } else {
      let presentNode = this.head;
      let arr = [];
      while (presentNode) {
        arr.push(presentNode.data);
        presentNode = presentNode.next;
      }
      return arr;
    }
  }

  pairwiseSwap() {
    var currentNode = this.head;
    if (currentNode == null) {
      return;
    }
    while (currentNode != null && currentNode.next != null) {
      [currentNode.data, currentNode.next.data] = [
        currentNode.next.data,
        currentNode.data
      ];

      currentNode = currentNode.next.next;
    }
  }
}

let linked_list = new LinkedList();
linked_list.add(1);
linked_list.add(3);
linked_list.add(5);
linked_list.add(2);
linked_list.add(9);
linked_list.add(4);

linked_list.pairwiseSwap();
console.log(linked_list.printElement());

// let a = 4;
// let b = 6;

// [b, a] = [a, b];

// console.log(a, b);
